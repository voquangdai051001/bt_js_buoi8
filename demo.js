// *****SỐ NGUYÊN*****
var number = [];
var numberThayDoiViTri=[]
var numberTangDan = [];
var daySNT = [];

document.getElementById("txt_themSoNguyen").onclick = function () {
  var inbut = document.getElementById("txt_nhapSoNguyen").value * 1;
  number.push(inbut);
  document.getElementById('txt_nhapSoNguyen').value=''
  document.getElementById("txt_cacSoNguyenVuaNhap").innerHTML = number;
  numberTangDan.push(inbut);
  numberThayDoiViTri.push(inbut)

  numberTangDan.sort(function (a, b) {
    return a - b;
  });


  var tongCacSoDuong = 0;
  var numberduongtangdan = [];
  var soNhoNhat = Math.min.apply(Math, number);
  var soChanCuoiCung = 0;
  var soLuongSoDuong = 0;
  var soLuongSoAm = 0;

  // Thay đổi vị trí 
  document.getElementById('txt_thaydoivitri').onclick=function(){
    var soThuTu1=document.getElementById('txt_viTri1').value*1
    var soThuTu2=document.getElementById('txt_viTri2').value*1
    var a=numberThayDoiViTri[soThuTu1-1]
    var b=numberThayDoiViTri[soThuTu2-1]
    numberThayDoiViTri[soThuTu2-1]=a
    numberThayDoiViTri[soThuTu1-1]=b
    document.getElementById('txt_daySoNguyenSauKhiThayDoi').innerHTML=numberThayDoiViTri
  }


  for (var i = 0; i < numberTangDan.length; i++) {
    var numberduong = numberTangDan[i];

    if (numberduong > 0) {
      soLuongSoDuong++;
      tongCacSoDuong += numberduong;
      numberduongtangdan.push(numberduong);
    } else {
      soLuongSoAm++;
    }
    if ((numberduong > 0) & (numberduong % 2 == 0)) {
      soChanCuoiCung = numberduong;
    }
  }
  //  số nguyên tố
  for (var k = 0; k < number.length; k++) {
    var sNT = number[k];
    kiem_tra_snt(sNT);
    function kiem_tra_snt(n) {

      var flag = true;

      // Nếu n bé hơn 2 tức là không phải số nguyên tố
      if (n < 2) {
        flag = false;
      } else if (n == 2) {
        flag = true;
      } else if (n % 2 == 0) {
        flag = false;
      } else {
        // lặp từ 3 tới n-1 với bước nhảy là 2 (i+=2)
        for (var i = 3; i < n - 1; i += 2) {
          if (n % i == 0) {
            flag = false;
            break;
          }
        }
      }

      // Kiểm tra biến flag
      if (flag == true) {
        daySNT.push(n);
      }
    }
  }

  document.getElementById("txt_tongCacSoDuong").onclick = function () {
    document.getElementById(
      "result"
    ).innerText = ` Tổng các số dương: ${tongCacSoDuong}`;
  };

  document.getElementById("txt_cacSoDuongNhap").onclick = function () {
    document.getElementById(
      "result"
    ).innerText = ` Các số dương vừa nhập: ${numberduongtangdan}`;
  };

  document.getElementById("txt_soNhoNhat").onclick = function () {
    document.getElementById("result").innerText = ` Số nhỏ nhất: ${soNhoNhat}`;
  };

  document.getElementById("txt_soDuongNhoNhat").onclick = function () {
    document.getElementById(
      "result"
    ).innerText = ` Số dương nhỏ nhất : ${numberduongtangdan[0]}`;
  };

  document.getElementById("txt_soChanCuoiCung").onclick = function () {
    document.getElementById(
      "result"
    ).innerText = ` Số chẳn cuối cùng: ${soChanCuoiCung}`;
  };

  document.getElementById("txt_sapXepTangDan").onclick = function () {
    document.getElementById(
      "result"
    ).innerText = ` Thứ tự tăng dần: ${numberTangDan}`;
  };

  document.getElementById("txt_sntDauTien").onclick = function () {
    if (daySNT.length > 0) {
      document.getElementById(
        "result"
      ).innerText = ` Số nguyên tố đầu tiên: ${daySNT[0]}`;
    } else {
      document.getElementById("result").innerText = ` -1`;
    }
  };

  document.getElementById("txt_sosanhSoDuongvaSoAm").onclick = function () {
    if (soLuongSoDuong > soLuongSoAm) {
      document.getElementById(
        "result"
      ).innerText = ` Số dương: ${soLuongSoDuong} nhiều hơn số âm: ${soLuongSoAm}`;
    } else if (soLuongSoDuong == soLuongSoAm) {
      document.getElementById("result").innerText = ` Số dương bằng số âm`;
    } else {
      document.getElementById(
        "result"
      ).innerText = ` Số âm: ${soLuongSoAm} nhiều hơn số dương: ${soLuongSoDuong}`;
    }
  };
  console.log("numberduongtangdan: ", numberduongtangdan);
  // ***
};

// ***** SỐ THỰC *****
var daySoThuc = [];
var soLuongSoNguyen=0
document.getElementById("txt_themSoThuc").onclick = function () {

  var nhapSoThuc = document.getElementById("txt_nhapSoThuc").value * 1;
  daySoThuc.push(nhapSoThuc);
  document.getElementById('txt_nhapSoThuc').value=''

  document.getElementById("txt_cacSoThucVuaNhap").innerHTML = daySoThuc;

  for (var l = 0; l < daySoThuc.length; l++) {
    var soThuc = daySoThuc[l];
    var soNguyen=Number.isSafeInteger(soThuc)
    if (soNguyen==true){
        soLuongSoNguyen++
    }
  }

  document.getElementById('txt_soLuongSoNguyen').onclick=function(){
    document.getElementById('result').innerText= ` Số lượng số nguyên: ${soLuongSoNguyen}`
  }


};
